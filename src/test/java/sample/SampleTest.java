package sample;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.impl.gradle.Gradle;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


@RunWith(Arquillian.class)
public class SampleTest {

    @Deployment
    public static Archive<?> createTestArchive() {
        Archive[] lib = Gradle.resolver()
                              .forProjectDirectory(".")
                              .importCompileAndRuntime()
                              .resolve()
                              .as(JavaArchive.class);

        return ShrinkWrap.create(WebArchive.class, "sample.war")
                         .addPackages(true, Sample.class.getPackage())
                         .addAsLibraries(lib);
    }

    @Test
    @RunAsClient
    public void test() throws Exception {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                new URL("http://localhost:8181/sample/").openStream()))) {
            String res = reader.readLine();
            assertThat(res, is("[\"hoge\",\"piyo\",\"fuga\"]"));
        }
    }

}