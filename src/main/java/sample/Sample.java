package sample;

import javax.json.Json;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/")
public final class Sample extends HttpServlet {

    private static final long serialVersionUID = -788769102711109786L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.getWriter().append(
                Json.createArrayBuilder()
                    .add("hoge")
                    .add("piyo")
                    .add("fuga")
                    .build().toString()
        );

    }
}
